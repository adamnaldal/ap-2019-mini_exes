# Mini_Ex 4 - social [Me]dia #  
![screenshot](program.png)  
https://gl.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex4/empty-example/index4.html  
Sourcecode is in the folder 'empty example' as Class4.js.  

I made a program which uses the .DOM-elements to, hopefully, make the recipient aware of the complexity of the data traces that they leave online. 
I have build it up as a social media in the Facebook blue, to underline the associations that I wanted the recipient to get. 
The program is not ment to scare people from using social media, but my idea is that it will make people aware of their ways online. 
I want the recipient to consider their feelings regarding data and data-storing, and take a stand. 
Whether that be to keep their privacy offline or to not care does not matter, the important thing (in my opinion) is to know that data-storing exists and is used.  

In the text Button by Søren Pold he writes:  
*"It is a software simulation of a function, and this simulation aims to hide its mediated character and acts as if the func- tion were natural or mechanical in a straight cause-and-effect relation. Yet it is anything but this: it is conventional, coded, arbitrary, and representational, and as such also related to the cultural."* (Pold, Fuller, p. 33)  

In my use of the buttons I have tried to take this in to consideration by writing 'stalk through my messages', 'spy on me' and 'find me' on them. 
My hope is that this will remove some of the illusion that a button is just a button.
Connected to these buttons are my private facebook-messages, a simulation of webcam footage (which sadly does not work, because I cannot push all the videos to gitlab), and my contact information.
The directness of the button-texts can maybe give the recipient an emotion of some sort. Maybe a surveillance fear will come up, maybe an urge to sneak a glimpse of a private life.  

Syntax wise I focused on the .DOM-elements this time, and really had fun and frustration playing around with them. 
- I use the createCapture to make a profile for the recipient.
- I use the createInput to make their name controlable
- I use the createSlider to make a gender-fluidity scale. 
    Which I found fun after having a classtalk about the gender radiobuttons not being inclusive enough. 
    But as we found out when we read the text about emojis: Sometimes trying to be more inclusive can exclude someone else. 
    And I think that has happened to me. 
    Trying to make a gender scale I found difficult because of the linear way of it. Having cis in one end and queer in the other is not a fulfilling picture of genders.

The other "part" of my program is based in my own personal life. I wanted it to be as private as possible, to make the clearest statement.
Here I used the button 'Add friend' to reviel the rest of my profile. This I did to create the same illusion of privacy as e.g. Facebook does. But here the recipient gets a little look behind the curton.
When the profile is revieled i have used these syntaxes:
- I use the createImg to give myself a profile picture
- I use the createDiv to give myself a name and later on for having messages and contact info
- I use the createButton to open up more of my profile
- I use the createVideo to simulate that the recipient gains access to my webcam. But again it does not work.

I have made a short video of the usage of my fake webcam-videos. See here: [Explanation](https://www.youtube.com/watch?v=AYaJr6KPHZY&feature=youtu.be)