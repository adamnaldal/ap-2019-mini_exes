**Ord af foråret // Words of Spring**

https://glcdn.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex7/empty-example/index77.html
https://gl.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex7/empty-example/index77.html 
https://cdn.staticaly.com/gl/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex7/empty-example/index77.html
(For some reason it keeps loading. You are more than welcome to try all three links. If they do not work, here is a video of it instead:  
https://www.youtube.com/watch?v=6VkJlWSYv84&feature=youtu.be)  
![screenshot](beckett.png)  

The rules of my program:
Using the example of Langton's Ant as interpreted by Winnie Soon
I have decided to make a program following these rules:
  1. Check status of current position.
  2. If position is empty generate a word, and move along.
  3. If position is full change word, and move along.
  4. Repeat at new position.

This week I have created a generative program that builds out of Winnie's source code of Langton's Ant.  
I became inspired to make a program of words. 
Looking through the material for this week, I came across this program: http://www.generative-gestaltung.de/2/sketches/?01_P/P_2_3_3_01.
I found it very beautiful. 
Reading the text "Randomness". 10 PRINT CHR$(205.5+RND(1)) by Nick Montfort et.al. I came across George Brecht's definitions of randomness:  

*"One [definition] where the origin of images is unknown because it lies in deeper-than-conscious levels of the mind and a second where images derive from mechanical processes not under the artist’s control." - p. 124*  

The second one he calls Dadaistic, and I decided to go that way with my program.
So the program creates the meaning for me, and the words is the beginning of my favorite piece by Samuel Beckett 'NOT I', (1973). 
An eight minute long monologue in a flashing, confusing, punctuational language (see bottom link).  
Choosing Beckett felt natural, because there is something almost generative about his writing, and especially this piece.
It has a mysterious mix of something very fluent and at the same time stacato to it. 
The thing is, that with becket you never know where he will go, except for when his texts loop. 
This is particularly visible in the piece of the mouth.
I also connect Beckett and generativity regarding the "rules". In generative programs there can be a feeling of randomness to it, but it is always controlled by a rule set.
The same applies to Becketts plays. Because of the fast pace or very very slow pace that is always a key aspect of his works,
an audience will most likely feel like the words are created in the mouth of the actor / actress. 
But Beckett uses A LOT of stage directions, and when his plays are set up, even today, it has to be exactly as in the manuscript, because his "rules" creates a specific kind of room or atmosphere.  
You actually have to contact the Beckett Foundation if you want to tweak anything, so I am probably breaking a copyright law right now :)

Regarding the technical aspects of my program, I first tried to make it from scratch myself.
But I struggled with removing the previous words, so that they did not layour on top of each other.
Therefore I "stole" Winnie's source code, and tweaked and added until it fitted my wishes.  
I have chosen to focus on practising for-loops and 2D Arrays.
In my practice sheets I played a lot with the random function, and I also played around with Winnie's code, until I think I understood most of it.  
  
  
  
  
  
  
  
  
  //  
Montfort, N, et al. "Randomness". 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, The MIT Press, 2012, pp. 119-146 (The chapter: Randomness)
Beckett, S, "NOT I". Repertory Theater, Samuel Beckett Festival, Lincoln Center, New York  https://www.youtube.com/watch?v=M4LDwfKxr-M