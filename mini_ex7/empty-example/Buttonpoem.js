let one=['her','alt','træk','den','øj','rul'];
let two=['står','hvad','ke','der','ne','ler'];
let three=['jeg','vi','vejr','elsk','svid','i'];
let four=['Ja','byg','et','e','er','mørk'];
let five=['son','ger','ud','de','råd','et'];
let one2=['slæb','øj','kvæl','bær','jeg','vild'];
let two2=['er','ne','es','er','brænd','and'];
let three2=['dig','svid','i','min','er','en'];
let four2=['gen','er','den','krop','mig','på'];
let five2=['nem','i','mørk','mod','på','brak'];
let six2=['nat','for','ke','Kre','råd','mark'];
let seven2=['ten','agt','nat','on','en','er'];
let one3=['med','brænd','skrig','brænd','nat','de'];
let two3=['en','er','er','er','ten','seks'];
let three3=['ny','dig','dit','dit','mist','æl'];
let four3=['kor','med','navn','ej','er','ling'];
let five3=['inth','dem','ud','e','jeg','er'];
let index=0;

function setup() {
  createCanvas(1390,700);
  butone=createButton(one[index]);
  butone.position(200,200);
  buttwo=createButton(two[index]);
  buttwo.position(250,200);
  butthree=createButton(three[index]);
  butthree.position(300,200);
  butfour=createButton(four[index]);
  butfour.position(350,200);
  butfive=createButton(five[index]);
  butfive.position(400,200);
  butone2=createButton(one2[index]);
  butone2.position(200,250);
  buttwo2=createButton(two2[index]);
  buttwo2.position(250,250);
  butthree2=createButton(three2[index]);
  butthree2.position(300,250);
  butfour2=createButton(four2[index]);
  butfour2.position(350,250);
  butfive2=createButton(five2[index]);
  butfive2.position(400,250);
  butsix2=createButton(six2[index]);
  butsix2.position(450,250);
  butseven2=createButton(seven2[index]);
  butseven2.position(500,250);
  butone3=createButton(one3[index]);
  butone3.position(200,300);
  buttwo3=createButton(two3[index]);
  buttwo3.position(250,300);
  butthree3=createButton(three3[index]);
  butthree3.position(300,300);
  butfour3=createButton(four3[index]);
  butfour3.position(350,300);
  butfive3=createButton(five3[index]);
  butfive3.position(400,300);
}

function draw() {
  background(0);
  fill(0);
  butone.mousePressed(wordOne);
  butone.style('background-color','#000000');
  butone.style('color','#FFFFFF');
  butone.style('padding','10px 17px');
  butone.style('border','#000000');
  fill(0);
  buttwo.mousePressed(wordTwo);
  buttwo.style('background-color','#000000');
  buttwo.style('color','#FFFFFF');
  buttwo.style('padding','10px 17px');
  buttwo.style('border','#000000');
  fill(0);
  butthree.mousePressed(wordThree);
  butthree.style('background-color','#000000');
  butthree.style('color','#FFFFFF');
  butthree.style('padding','10px 17px');
  butthree.style('border','#000000');
  fill(0);
  butfour.mousePressed(wordFour);
  butfour.style('background-color','#000000');
  butfour.style('color','#FFFFFF');
  butfour.style('padding','10px 17px');
  butfour.style('border','#000000');
  fill(0);
  butfive.mousePressed(wordFive);
  butfive.style('background-color','#000000');
  butfive.style('color','#FFFFFF');
  butfive.style('padding','10px 17px');
  butfive.style('border','#000000');
  fill(0);
  butone2.mousePressed(wordOne2);
  butone2.style('background-color','#000000');
  butone2.style('color','#FFFFFF');
  butone2.style('padding','10px 17px');
  butone2.style('border','#000000');
  fill(0);
  buttwo2.mousePressed(wordTwo2);
  buttwo2.style('background-color','#000000');
  buttwo2.style('color','#FFFFFF');
  buttwo2.style('padding','10px 17px');
  buttwo2.style('border','#000000');
  fill(0);
  butthree2.mousePressed(wordThree2);
  butthree2.style('background-color','#000000');
  butthree2.style('color','#FFFFFF');
  butthree2.style('padding','10px 17px');
  butthree2.style('border','#000000');
  fill(0);
  butfour2.mousePressed(wordFour2);
  butfour2.style('background-color','#000000');
  butfour2.style('color','#FFFFFF');
  butfour2.style('padding','10px 17px');
  butfour2.style('border','#000000');
  fill(0);
  butfive2.mousePressed(wordFive2);
  butfive2.style('background-color','#000000');
  butfive2.style('color','#FFFFFF');
  butfive2.style('padding','10px 17px');
  butfive2.style('border','#000000');
  fill(0);
  butsix2.mousePressed(wordSix2);
  butsix2.style('background-color','#000000');
  butsix2.style('color','#FFFFFF');
  butsix2.style('padding','10px 17px');
  butsix2.style('border','#000000');
  fill(0);
  butseven2.mousePressed(wordSeven2);
  butseven2.style('background-color','#000000');
  butseven2.style('color','#FFFFFF');
  butseven2.style('padding','10px 17px');
  butseven2.style('border','#000000');
  fill(0);
  butone3.mousePressed(wordOne3);
  butone3.style('background-color','#000000');
  butone3.style('color','#FFFFFF');
  butone3.style('padding','10px 17px');
  butone3.style('border','#000000');
  fill(0);
  buttwo3.mousePressed(wordTwo3);
  buttwo3.style('background-color','#000000');
  buttwo3.style('color','#FFFFFF');
  buttwo3.style('padding','10px 17px');
  buttwo3.style('border','#000000');
  fill(0);
  butthree3.mousePressed(wordThree3);
  butthree3.style('background-color','#000000');
  butthree3.style('color','#FFFFFF');
  butthree3.style('padding','10px 17px');
  butthree3.style('border','#000000');
  fill(0);
  butfour3.mousePressed(wordFour3);
  butfour3.style('background-color','#000000');
  butfour3.style('color','#FFFFFF');
  butfour3.style('padding','10px 17px');
  butfour3.style('border','#000000');
  fill(0);
  butfive3.mousePressed(wordFive3);
  butfive3.style('background-color','#000000');
  butfive3.style('color','#FFFFFF');
  butfive3.style('padding','10px 17px');
  butfive3.style('border','#000000');
}

function wordOne(){
  butone=createButton(one[index]);
  index=index+1;
  butone.position(200,200);
  if(index==one.length){
  index=0;
  }
}

function wordTwo(){
  buttwo=createButton(two[index]);
  index=index+1;
  buttwo.position(250,200);
  if(index==two.length){
  index=0;
  }
}

function wordThree(){
  butthree=createButton(three[index]);
  index=index+1;
  butthree.position(300,200);
  if(index==three.length){
  index=0;
  }
}

function wordFour(){
  butfour=createButton(four[index]);
  index=index+1;
  butfour.position(350,200);
  if(index==four.length){
  index=0;
  }
}

function wordFive(){
  butfive=createButton(five[index]);
  index=index+1;
  butfive.position(400,200);
  if(index==five.length){
  index=0;
  }
}

function wordOne2(){
  butone2=createButton(one2[index]);
  index=index+1;
  butone2.position(200,250);
  if(index==one2.length){
  index=0;
  }
}

function wordTwo2(){
  buttwo2=createButton(two2[index]);
  index=index+1;
  buttwo2.position(250,250);
  if(index==two2.length){
  index=0;
  }
}

function wordThree2(){
  butthree2=createButton(three2[index]);
  index=index+1;
  butthree2.position(300,250);
  if(index==three2.length){
  index=0;
  }
}

function wordFour2(){
  butfour2=createButton(four2[index]);
  index=index+1;
  butfour2.position(350,250);
  if(index==four2.length){
  index=0;
  }
}

function wordFive2(){
  butfive2=createButton(five2[index]);
  index=index+1;
  butfive2.position(400,250);
  if(index==five2.length){
  index=0;
  }
}

function wordSix2(){
  butsix2=createButton(six2[index]);
  index=index+1;
  butsix2.position(4500,250);
  if(index==six2.length){
  index=0;
  }
}

function wordSeven2(){
  butseven2=createButton(seven2[index]);
  index=index+1;
  butseven2.position(500,250);
  if(index==seven2.length){
  index=0;
  }
}

function wordOne3(){
  butone3=createButton(one3[index]);
  index=index+1;
  butone3.position(200,300);
  if(index==one3.length){
  index=0;
  }
}

function wordTwo3(){
  buttwo3=createButton(two3[index]);
  index=index+1;
  buttwo3.position(250,300);
  if(index==two3.length){
  index=0;
  }
}

function wordThree3(){
  butthree3=createButton(three3[index]);
  index=index+1;
  butthree3.position(300,300);
  if(index==three3.length){
  index=0;
  }
}

function wordFour3(){
  butfour3=createButton(four3[index]);
  index=index+1;
  butfour3.position(350,300);
  if(index==four3.length){
  index=0;
  }
}

function wordFive3(){
  butfive3=createButton(five3[index]);
  index=index+1;
  butfive3.position(400,300);
  if(index==five3.length){
  index=0;
  }
}
