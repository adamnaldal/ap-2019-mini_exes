/*Using the example of Langton's Ant as interpreted by Winnie Soon
I have decided to make a program following these rules:
  1. Check status of current position.
  2. If position is empty generate a word || if position is full change word, and move along.
  3. Begin at new position.*/

//Word-"ant"
let meBe=[' OUT','...',' INTO',' THIS',' WORLD',' ...',' TINY',' LITTLE',' THING',' ...',
' BEFORE',' ITS',' TIME',' ...',' IN',' A',' GODFOR-',' ...',' WHAT',' ?',' ...',
' GIRL',' ?',' ...',' YES',' ...',' TINY',' LITTLE',' GIRL',' ...',' INTO',
' THIS',' ...',' OUT',' INTO',' THIS',' ...',' BEFORE',' HER',' TIME',' ...',
' GODFORSAKEN',' HOLE',' CALLED',' ...',' CALLED',' ...',' NO',' MATTER',' ...',
' PARENTS',' UNKNOWN',' ...',' UNHEARD',' OF',' ...',' HE',' HAVING',' VANISHED',' ...',
' THIN',' AIR',' ...',' NO',' SOONER',' BUTTONED',' UP',' HIS',' BREECHES',' SHE',
' SIMILARLY',' ...',' EIGHT',' MONTHS',' LATER',' ...',' ALMOST',' TO',' THE',' TICK',
' ...',' SO',' NO',' LOVE',' ...',' SPARED',' THAT',' ...',' NO',' LOVE',
' SUCH',' AS',' NORMALLY',' VENTED',' ON',' THE',' ...',' SPEECHLESS',' INFANT',' ...',
' IN',' THE',' HOME',' ...',' NO',' ...',' NO',' INDEED',' FOR',' THAT',
' MATTER',' ANY',' OF',' ANY',' KIND',' ...',' NO',' LOVE',' OF',' ANY',
' KIND',' ...',' AT',' ANY',' SUBSEQUENT',' STAGE',' ...',' SO',' TYPICAL',' AFFAIR',
' ...',' NOTHING',' OF',' ANY',' NOTE',' TILL',' COMING',' UP',' TO',' SIXTY',
' WHEN-',' ...',' WHAT',' ?',' ...',' SEVENTY',' ?',' ...',' GOOD',' GOD',
' !',' ...',' COMING',' UP',' TO',' SEVENTY',' ...',' WANDERING',' IN',' A',
' FIELD',' ...',' LOOKING',' AIMLESSLY',' FOR',' COWSLIPS',' ...',' TO',' MAKE',' A',
' BALL',' ...',' A',' FEW',' STEPS',' THEN',' STOP',' ...',' STARE',' INTO',
' SPACE',' ...',' THEN',' ON',' ...',' A',' FEW',' MORE',' ...',' STOP',
' AND',' STARE',' AGAIN',' ...',' SO',' ON',' ...',' DRIFTING',' AROUND',' ...',
' WHEN',' SUDDENLY',' ...',' GRADUALLY',' ALL',' WENT',' OUT',' ...'];
let index=0;
//Titel
let youGo=['        I','        I','NOT ','NOT ']
let go;

//Birdsounds. Icelandic Geese :)
let fugle;

//Making of grid (100 = the rect-size)
let grid_space = 100;
let grid =[];
let cols, rows;
let xPos, yPos;
//Movement of word-Ant
let dir;
const antUP = 0;
const antRIGHT = 1;
const antDOWN = 2;
const antLEFT = 3;

function preload(){
  fugle=loadSound('fugle.m4a');
}

function setup(){
  //Slowing down the ants
  frameRate(1);
  createCanvas(1500,800);
  background(0);

  //See function further down
  grid = drawGrid();
  //Placement of ant-position
  xPos = floor(cols/2);
  yPos = floor(rows/2);
  dir = antUP;

  //Titel made with class
  go=new Life(100,100,100);
  //Loop sound
  fugle.loop();
}

function draw() {
  //"Invisable" rect for covering up texts
  fill(0);
  rect(0,0,400,200);
  //Titels
  fill(255);
  textSize(50);
  text('THE MOUTH',100,150);
  go.doSomething();

  //Creating grid and states
  for (let n = 0; n < 1; n++) {
    //See function further down
    checkEdges();
    let state = grid[xPos][yPos];
    //the first rule
    if (state == 0) {
      dir++;
      grid[xPos][yPos] = 1;
      textSize(10);
      fill(255);
      text(meBe[index]);
      fill(0);
      rect(xPos*grid_space+3, yPos*grid_space-20,grid_space,grid_space);
      //Change direction
      if (dir > antLEFT) {
        dir = antUP;
      }

    //Second rule
    }else{
      dir--;
      grid[xPos][yPos] = 0;
      textSize(10);
      fill(255);
      text(meBe[index++]);
      if(index>=72){
        index=0;
      }
      fill(0);
      rect(xPos*grid_space+3, yPos*grid_space-20,grid_space,grid_space);
      //Change direction
      if (dir < antUP) {
        dir = antLEFT;
      }
    }
    //The ant
    fill(255);
    text(meBe[index],xPos*grid_space, yPos*grid_space);
    //See function further down
    nextMove();
  }
}

function drawGrid() {
  cols = width/grid_space;
  rows = height/grid_space;
  //Creating the grid with for-loop
  let arr = new Array(cols);
  for (let i=0; i < cols; i++) {
    //2D array
    arr[i] = new Array(rows);
    for (let j=0; j < rows; j++){
      //Coordinates
      let x = i * grid_space;
      let y = j * grid_space;
      //For some reason a problem with doing noStroke();
      //so the stroke is the same colour as background
      stroke(0);
      strokeWeight(0.3);
      noFill();
      rect(x, y, grid_space, grid_space);
      arr[i][j] = 0;  // assign each cell with the status 0 / off
    }
  }

  return arr;
}

function nextMove () {
  //check which direction to go next and set the new current direction
  if (dir == antUP) {
    yPos--;
  } else if (dir == antRIGHT) {
    xPos++;
  } else if (dir == antDOWN) {
    yPos++;
  } else if (dir == antLEFT) {
    xPos--;
  }
}

function checkEdges() {
  //check width and height boundary
  if (xPos > cols-1) { //reach the right edge
    xPos = 0;    //go back to the left
  } else if (xPos < 0) {  //reach the left edge
    xPos = cols-1;  //go to the right edge
  }
  if (yPos > rows-1) { //reach the bottom edge
    yPos = 0; //go back to the top
  } else if (yPos < 0) { //reach the top edge
    yPos = rows-1;  //go to the bottom
  }
}

//Practicing classes
class Life{
  //I tried to change the variables to more "poetic" names, but it became confusing so I gave up
  constructor(it,we,love){
    this.ego=it;
    this.no=we;
    this.us=love;
  }

  doSomething(){
    fill(255);
    noStroke();
    textSize(this.us);
    textFont('Helvetica');
    text(random(youGo),this.ego,this.no);
    }
  }
