/*ændr den ene til en frisk smiley.
tilføj galde glidende op fra bunden.
stop galden når emojierne bliver glade igen*/
let smile
let frown
let smile2
let frown2
let brows
let brows2
let brows2b

function setup(){
  createCanvas(1100,windowHeight);
  background(0,0,139);
}

function draw(){
  //left box to cover text
  fill(0,0,139);
  noStroke();
  rect(270,30,300,50);

  //right box to cover text
  fill(0,0,139);
  noStroke();
  rect(550,30,300,50);

  //left head
  noStroke();
  fill(255,255,0);
  ellipse(400,300,200,200);
    //mouth
    noFill();
    stroke(0);
    strokeWeight(10);
    bezier(350,frown2,399,smile2,410,smile2,450,frown2);
    //left eye
    strokeWeight(0);
    fill(255);
    ellipse(360,280,15,15);
    fill(0);
    ellipse(360,280,7,7);
    //right eye
    fill(255);
    ellipse(435,280,15,15);
    fill(0);
    ellipse(435,280,7,7);
    //left eyebrow
    noFill();
    strokeWeight(5);
    bezier(340, brows2b, 354, brows2, 355, brows2, 370, 260);
    //right eyebrow
    bezier(420, 260, 434, brows2, 435, brows2, 450, brows2b);

    if(mouseY>=300){
      //lower eyebrows, smile
      textSize(32);
      noStroke();
      fill(255,192,203);
      text('Akward but happy', 570, 60);
      strokeWeight(5);
      brows=255;
      smile2=390;
      frown2=330;
    } else{
      //lift eyebrows, frown
      textSize(32);
      noStroke();
      fill(65,105,225);
      text('Doubting the world', 580, 60);
      brows=255;
      smile2=390;
      frown2=330;
    }

  //right head
  noStroke();
  fill(255,255,0);
  ellipse(700,300,200,200);
    //mouth
    noFill();
    stroke(0);
    strokeWeight(10);
    bezier(650,330,699,smile,700,smile,750,frown);
    //left eye
    strokeWeight(0);
    fill(255);
    ellipse(660,280,15,15);
    fill(0);
    ellipse(660,280,7,7);
    //right eye
    fill(255);
    ellipse(735,280,15,15);
    fill(0);
    ellipse(735,280,7,7);
    //left eyebrow
    noFill();
    strokeWeight(5);
    bezier(640, brows, 654, brows, 655, 250, 670, 250);
    //right eyebrow
    bezier(720, 250, 734, 250, 735, brows, 750, brows);

    if(mouseY>=300){
      //lower eyebrows, smile
      textSize(32);
      noStroke();
      fill(255,192,203);
      text('Fake emotion', 270, 60);
      strokeWeight(5);
      brows2=257;
      brows2b=260;
      smile=350;
      frown=330;
    } else{
      //lift eyebrows, frown
      textSize(32);
      noStroke();
      fill(65,105,225);
      text('Fake emotion', 270, 60);
      brows2=250;
      brows2b=240;
      smile=330;
      frown=340;
    }
  }
