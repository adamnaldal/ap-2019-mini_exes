//transparency and colour
trans=200;
col=200;
//movement
let drive=0
function setup(){
  createCanvas(1000,800);
}
function draw(){
  background(0);
  col=random(0,200);
  let y=350;
  let x=450;
  //looping the rects
  for(let y=350;y>250;y=y-15){
    fill(200,trans);
    rect(450,y-drive,10,10);
  }
  for(let x=450;x<550;x=x+15){
    rect(x+drive,y,10,10);
  }
  for(let y=350;y>250;y=y-15){
    rect(540,y+drive,10,10);
  }
  for(let x=450;x<550;x=x+15){
    rect(x-drive,260,10,10);
  }
  //moving the lines of rect() and moving them back to start position
  drive=drive+1
  if(drive>100){
    drive=0;
  }
  //making throbber disappear
  trans=trans-2;
  if(trans<=0){
    trans=200;
  }
  //rects for covering up the lines (:
  fill(0);
  rect(550,340,100,40);
  rect(430,160,40,100);
  rect(350,250,100,40);
  rect(530,360,40,100);
}
