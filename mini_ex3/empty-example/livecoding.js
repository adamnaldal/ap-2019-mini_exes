let capture;
let button;
function setup(){
  createCanvas(800,600)
  capture=createCapture();
  capture.hide();
  button=createButton('Press me!');
  button.position(600,600);
  button.mousePressed(BC);
  background(200,100,100);
}
function BC(){
  r=random(100,200);
  g=random(100,200);
  b=random(100,200);
  background(r,g,b);
}
function draw(){
  if(capture.loadedmetadata){
    face=capture.get(200,30,300,300);
    image(face,100,100);
  }
}
