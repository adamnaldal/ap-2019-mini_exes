let img;
let canvas;
let capture;
let button;
let r=200;
let g=100;
let b=100;
function setup(){
  canvas = createCanvas(800,600);
  canvas.position(100,100);
  capture=createCapture(VIDEO);
  capture.hide();
  img=createImg("https://scontent.fbll1-1.fna.fbcdn.net/v/t1.0-9/51567836_10218811311018288_5488804213846704128_o.jpg?_nc_cat=109&_nc_ht=scontent.fbll1-1.fna&oh=52beb1263958e408fe6bc5df33a40b9b&oe=5CEFE626");
  img.position(0,500);
  img.size(400,200);
  background(200,100,100);
  button=createButton('Change Background');
  button.position(700,500);
  button.mousePressed(changeBG);
}
function changeBG(){
  r=random(100,200);
  g=random(100,200);
  b=random(100,200);
  background(r,g,b);
}
function draw(){
  if(capture.loadedmetadata){
    let face=capture.get(70,30,400,400);
    image(face,150,100);
  }
  noStroke();
  fill(0,0,200);
  ellipse(mouseX,mouseY,20,20)
}
