var col={
  r:238,
  g:232,
  b:170
};
let yellow=200;
let heat=2;
let x=-500;
let y=270;
let speed=50;
let starman;
let reverb;

function preload(){
  soundFormats('mp3');
  starman=loadSound('starman.mp3');
}

function setup() {
  starman.play();
  // WEBGL used when 3D
  createCanvas(windowWidth,windowHeight, WEBGL)
  //removed cursor for artistic look :)
  noCursor();
}

function draw() {
  background(color(0,0,100));

  //stars
  push();
  translate(-700,-500);
  for(let henad=200;henad<=width;henad+=130){
    for(let opad=200;opad<=2000;opad+=70){
      noStroke();
      fill(col.r,col.g,random(255));
      ellipse(random(henad),random(opad),3,3);
    }
  }
  pop();

  //comet
  push();
  noStroke();
  fill(230,230,170);
  ellipse(x,270,30,4);
  if(x>10000){
    x=-500;
  }
  x=x+speed;
  pop();

  push();
  //using the unfilled beziers for creating wavey milkeyway
  noFill();
  //stroke = color of line
  stroke(250);
  bezier(-750,200,-400,1000,0,300,1000,-100);
  noFill();
  stroke(250);
  bezier(750,-200,400,-1000,0,-300,-1000,100);
  pop();

  //the Sun
  fill(255,yellow,0);
  sphere(100,200,200);
  yellow=yellow+heat;
  if(yellow>250){
    heat=-2;
  }else if(yellow<200){
    heat=+2;
  }

  //Mercury
  push();
  rotateZ(frameCount*0.001);
  noStroke();
  fill(0,50,120);
  translate(-20,120);
  sphere(10,10);
  translate(1,1,1);
  fill(100,0,100,100);
  sphere(10,10);
  pop();

  //Venus
  push();
  rotateZ(frameCount*0.05);
  noStroke();
  fill(255,170,0);
  translate(-20,150);
  sphere(20,20);
  pop();

  //Earth
  push();
  rotateZ(frameCount*0.01);
  noStroke();
  fill(0,50,120);
  translate(-20,190);
  sphere(15,15,200);
  translate(1,0.5);
  fill(0,100,100);
  sphere(15,15,200);
  pop();

  //Mars
  push();
  rotateZ(frameCount*0.009);
  noStroke();
  fill(255,50,70);
  translate(-20,220);
  sphere(13,13);
  pop();

  //Jupiter
  push();
  rotateZ(frameCount*0.017);
  noStroke();
  fill(130,100,50);
  translate(-20,280);
  sphere(50,50);
  pop();

  //Saturn
  push();
  rotateZ(frameCount*0.015);
  translate(-20,400);
  noStroke();
  fill(200,200,100);
  sphere(42,42);
  rotateY(frameCount*0.03);
  fill(20,20,0,100);
  torus(70,10,200,200);
  pop();

  //Uranus
  push();
  rotateZ(frameCount*0.02);
  noStroke();
  fill(0,120,200);
  translate(-20,510);
  sphere(35,35);
  rotateY(frameCount*0.03);
  fill(255,20);
  torus(45,1);
  pop();

  //Neptune
  push();
  rotateZ(frameCount*0.008);
  noStroke();
  fill(20,20,200);
  translate(-20,530);
  sphere(30,30);
  pop();

  //Neptune
  push();
  noStroke();
  fill(60,60,40);
  translate(300,300);
  sphere(4);
  pop();
}
