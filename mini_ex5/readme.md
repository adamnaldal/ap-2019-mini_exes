![screenshot](mini_ex1_Planet.png)
![screenshot](planetny.png)

**Mini_Ex1 revisited**  

https://glcdn.githack.com/Pomelofrugter/ap-2019-mini_exes/raw/master/mini_ex5/empty-example/index5.html

My first thought with the revisit was technical. I had a hard time understanding translate() and push()pop(), 
and I also wanted to try to remember the syntax for for loops and arrays. 
So I went back to my very first program that was saturn on a blue canvas, where I had put in stars in the background one at a time.
This time I went for our whole solar system.
I used translate to make the planets rotate around the Sun. 
I used push()pop() to limit the different parts, so they wouldn't affect each other. 
Then I played around with the for loop, trying to create a background of stars blinking around on the screen.
It did not end the way I had planned, but I kind of like how it looks now.  
I also research the planets distance to and rotation around the Sun, to get it as realistic as possible. 

The reason for revisiting mini_ex1 is that I remember being really frustrated about the stars not being able to get drawn in one line of code.
It did not really work this time either, but it was important to me, to bring this frustration back and see if I could "solve" it. 
So this program became about having visions --> letting the frustration out about the vision not coming true in my program --> playing around with the code --> getting to a new point that I liked --> making that inspire me.  

I also wanted to try using the sound library. Both to extend my ability but also because I had a lot of trouble with the createVideo, and wanted to see if my browser was just as unaccepting when it came to sound.
It is :( I cannot get Starman (David Bowie) to play in chrome, safari or firefox, as soon at it is in my gitlab. 
When I just go onto the index file from the folder on my desktop, there is no problem? 
New frustration right there. 

When I had the piece with the solar system finished, it frustrated me that there wasn't really anything to it conceptually. 
I, hopefully, changed this by adding a whole new side of the program.
At the buttom there is a round, red button saying 'Press for Big Bang'. 
When the button is pushed the user creates "the big bang" and enters a new page where I tried simulating a hackers version of them playing God.  
Inspired by a "hacking" webpage I came across, I wanted to make a line, where the user could write themselves. 
It does not alter the program though.
It is pure illusion :)
Using signs and numbers in a line saying: "Start c0d1ng h3r3" (or something like that) was to give it a humorous tone (knowing a tiny bit about code, you can easily see that it has nothing to do with code).
I have also added a piece of my own text about harmagedon to add to the doomsday feel.
I have kept my text in Danish because I would like it to be seen as another piece of mystery: giving it some sence of esoteric language.
I struggled with getting the program to actually show more text than just 'Harmagedon'.
I want it to show a piece of text, wait 5 seconds, show the next piece and so on. 
I tried creating arrays and using the setInterval() where in I made my own function. 
But I must be doing something wrong, because only "Harmagedon" appears.

Find my sourcecode in the folder empty-examples as Class5.