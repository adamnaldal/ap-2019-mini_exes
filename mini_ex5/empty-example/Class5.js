var col={
  r:238,
  g:232,
  b:170
};
let yellow=200;
let heat=2;
let x=-500;
let y=270;
let speed=50;
let starman;
let reverb;
let button;
let pulse;
let av;
let hack;
let hackdiv;
//poem divided with array
let harmagedon=['Harmagedon',
'Himlen er i et landskab af sne. Sneen på din handske. Sneen falder på dig. Vintergækkerne bryder gennem jorden. Du går gennem sneen. Du går gennem vintergækkerne. Du mærker det knase under dine tunge støvler. Du mærker sneens stilhed. Du ser på dine fodspor. Helt vildt stolt. Tænk at du kan sætte fodspor lige her. Du hører noget pusle. Du hører et højt kort skrig. Du ser kragen lette. Fuglen flyver over byen – du skyder den ned. I en snor om halsen slæber du den gennem sneen. Du ser den slæbende linje. Så stolt. Du flår fjerene af. Du tænder for blusset. Flammen der bruser ud af ingentinget. Det syder i gryden. Fuglen ender i munden. Du rækker hånden frem. Du hælder vinen op. Du sidder lidt. Alene ved bordet. Du keder dig. Du drikker mere vin. Der er mange dage tilbage, tænker du. Du tænder for varmen og åbner dit vindue. Snuser privilegiet ind. Du ser dig om i din stue. Din smukke lyse stue. Du ser et hul et sted bag reolen. Som ind til en anden verden.',
'Noget nyt – en forandring.',
'Du ser en samling af noget grønt, ligesom blomster. Måske er det venner. En dag vil du ligge under et træ, tænker du. Du sender et blik mod jorden. Træerne lyser. Igen ser du fuglen på himlen. Himlen, som kun er et loft over dig. Bygningerne er skabe med tøj, du tager på. Et blik på dig selv.',
'Nu: Et andet blik på dig.',
'Du tager træerne i hænderne. River rødderne op. Nogle flækker på vejen. Dagene er gule og hvide, før var de grønne. Den krassende lyd af kniv mod tallerken. Du skraber tallerkenen fri for fugl og lækker sovs. Du fornemmer lyset komme gennem væggene nu. Lyset smelter gennem væggen – så stærkt går det. Skyerne over dig er store. Bjerget foran dig er vigtigt. Bygget mens solen stod op.',
'Tænk at gå fremad.',
'Det er en helt anden verden. Så er det bedre at hvile sine lår mod de kolde aluminiumsstole på cafeen. Du sidder der og tænker: Du kunne da godt gå fremad. Himlen åbner sig. Himlen er over cafeens tag, det ved du. Du fornemmer fuglen, der flyver over taget. Lyden af vingerne er kortslutningen. Bladene falder af træerne. Himlen er åben. Solen går ned over bjergene ligesom dit smil. Lyset på cafeen var falsk, ved du nu. Himlen er tom.',
'Jorden er under dig. Du ser den fra oven nu. Her står himlen i røg. Du kan se den brænder. Flammerne overtager. Nu er røgen tyk over havet. Du skriger: Himlen er så stor. Du kaster armene mod den. Du mærker universet som et brøl. Følelsen, som når man kører på motorcykel. Rummet overtager dig. Kniven kastes rundt. Et opkald fra rummet til jorden. Se mig, mærk mig, kend mig. En form for blitz gennem mørket. Du er klædt i hvidt nu.',
'Efterveerne: Den rolige himmel.',
'Du tænker på din barndom. Du tænker på blomsterne – de gule og lyserøde. Du tænker på himlen. Du tænker på havet. Du tænker på stenene. Du tænker på dine venner. Du tænker på isbjørnene. Du husker en sommerhustur og forelskelsen. Lysene kommer ud af mørket. Solen er der bag træerne, rolig nu. Du står på dine fødder, og latteren buldrer i dig. Du ser op på himlen. Aldrig beklage sig igen, tænker du. Du begynder at bevæge dig. Lyset er ikke længere som en blitz. Nu roligt.',
'Der vokser en orkide ved dine fødder.']
let op=0;

function preload(){
  soundFormats('mp3');
  //load David Bowie
  starman=loadSound('starman.mp3');
  //a picture of black background. the only way I could figure it out
  bigBang=createImg('http://ee.eobuildingstone.com/uploads/201815292/small/g685-black-granite-slab24315011974.jpg');
  bigBang.hide();
  bigBang.size(2000,1500);
}

function setup() {
//start Bowie
  starman.play();
  // WEBGL used when 3D
  createCanvas(windowWidth,windowHeight, WEBGL)
  
  //big bang button
  button=createButton('PRESS FOR BIG BANG');
  button.position(50,600);
  button.style('background','#B22222');
  button.style('border','none');
  button.style('font-size','15px');
  button.style('padding','70px 5px');
  button.style('border-radius','50%');
  button.mousePressed(boom);
  
  //hacking page after big bang
  hack=createInput('... b3g1n ur hÃck1ng h3r3: [ ]');
  hack.hide();
  hackdiv=createDiv(harmagedon[op]);
  hackdiv.hide()

  //Create and start the pulse wave oscillator
  pulse = new p5.Pulse();
  av=pulse.freq(50);
  pulse.stop();
}

function boom(){
  starman.stop();
  //start noise
  pulse.start();
  //black screen
  bigBang.position(-300,-200);
  bigBang.show();
  button.hide();
  //inputroom for user
  hack.show();
  hack.position(5,5);
  hack.size(1400,30);
  hack.style('font-size','20px');
  hack.style('color','#00FF00');
  hack.style('border','hidden');
  hack.style('background','	#000000');
  //poem
  hackdiv.show();
  hackdiv.position(900,100);
  hackdiv.style('font-size','20px');
  hackdiv.style('color','#00FF00');
  //wait 5 seconds, go to next piece of poem. BUT WILL NOT WORK!?
  setInterval(tekst,5000);
}

function draw() {
  background(0);

  //stars
  push();
  translate(-700,-500);
  for(let henad=200;henad<=width;henad+=130){
    for(let opad=200;opad<=2000;opad+=70){
      noStroke();
      fill(col.r,col.g,random(255));
      ellipse(random(henad),random(opad),3,3);
    }
  }
  pop();

  //comet
  push();
  noStroke();
  fill(230,230,170);
  ellipse(x,270,30,4);
  if(x>10000){
    x=-500;
  }
  x=x+speed;
  pop();

  push();
  //using the unfilled beziers for creating wavey milkeyway
  noFill();
  //stroke = color of line
  stroke(250);
  bezier(-750,200,-400,1000,0,300,1000,-100);
  noFill();
  stroke(250);
  bezier(750,-200,400,-1000,0,-300,-1000,100);
  pop();

  //the Sun
  fill(255,yellow,0);
  sphere(100,200,200);
  yellow=yellow+heat;
  if(yellow>250){
    heat=-2;
  }else if(yellow<200){
    heat=+2;
  }

  //Mercury
  push();
  rotateZ(frameCount*0.001);
  noStroke();
  fill(0,50,120);
  translate(-20,120);
  sphere(10,10);
  translate(1,1,1);
  fill(100,0,100,100);
  sphere(10,10);
  pop();

  //Venus
  push();
  rotateZ(frameCount*0.05);
  noStroke();
  fill(255,170,0);
  translate(-20,150);
  sphere(20,20);
  pop();

  //Earth
  push();
  rotateZ(frameCount*0.01);
  noStroke();
  fill(0,50,120);
  translate(-20,190);
  sphere(15,15,200);
  translate(1,0.5);
  fill(0,100,100);
  sphere(15,15,200);
  pop();

  //Mars
  push();
  rotateZ(frameCount*0.009);
  noStroke();
  fill(255,50,70);
  translate(-20,220);
  sphere(13,13);
  pop();

  //Jupiter
  push();
  rotateZ(frameCount*0.017);
  noStroke();
  fill(130,100,50);
  translate(-20,280);
  sphere(50,50);
  pop();

  //Saturn
  push();
  rotateZ(frameCount*0.015);
  translate(-20,400);
  noStroke();
  fill(200,200,100);
  sphere(42,42);
  rotateY(frameCount*0.03);
  fill(20,20,0,100);
  torus(70,10,200,200);
  pop();

  //Uranus
  push();
  rotateZ(frameCount*0.02);
  noStroke();
  fill(0,120,200);
  translate(-20,510);
  sphere(35,35);
  rotateY(frameCount*0.03);
  fill(255,20);
  torus(45,1);
  pop();

  //Neptune
  push();
  rotateZ(frameCount*0.008);
  noStroke();
  fill(20,20,200);
  translate(-20,530);
  sphere(30,30);
  pop();

  //Neptune
  push();
  noStroke();
  fill(60,60,40);
  translate(300,300);
  sphere(4);
  pop();
}

//poem. go to next paragraph. NOT WORKING.
function tekst(){
  op=op+1;
  if(op==harmagedon.length){
    op=0;
  }
}
