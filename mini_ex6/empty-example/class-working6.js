class Can{
  constructor(){
    this.x=300;
    this.y=600;
    this.wide=120;
    this.high=200;
  }
  right(){
    this.x=this.x+5;
  }
  left(){
    this.x=this.x-5;
  }
  catch(){
    this.wide=this.wide+5;
    this.high=this.high+2;
  }
  show(){
    stroke(200);
    fill(160,140,0);
    ellipse(this.x,this.y,this.wide,this.high);
    noStroke();
    fill(0);
    ellipse(270,570,20,20);
    ellipse(330,570,20,20);
    ellipse(300,630,40,20);
  }
}

class Trash {
  constructor(speed, xpos, ypos, size) {
    this.speed = speed;
    this.pos = new createVector(xpos, ypos);
    this.size = size;
  }

  move() {
    this.pos.y +=this.speed;
    if (this.pos.y > 630) {
      this.pos.y = 0;
    }
  }

  show() {
    fill(120,90,0);
    noStroke();
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
  }
}
